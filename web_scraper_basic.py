import sys
import urllib.request
import re
from urllib.parse import urljoin
from bs4 import BeautifulSoup as bs
import time

# List of Regexr's
emailRegex = r"[\w.]+@\w+.\w+";
tlfRegex = r"\+45[0-9 ]+"
addRegex = r"[1-4][0-9][0-9][0-9]\s[A-Z][a-zæøå]+"

add_list = [];
tlf_list =[];
email_list = [];

file = 'Firma_Hjemmesideliste.csv'
output = 'Output.csv';

""" Description of program
Load in file with list of company websites.
Begin with three booleans false
For each website, search the site and subsites for the three informations, saving the found information, 
and setting the flags true, indicating that a match was found.
Writes a line to file, with:
Website, E-mail, Telephone, Address, E-mail
"""
#line_list = list(filter(None, soup.get_text().split('\n')));

def text2list(text):
	return list(filter(None, text.split('\n')));
	
def find_information():
	for line in open(file, 'r'):
		print(line);
		base, link = line, '';
		(email, tlf, address) = (None, None, None)
		try:
			(text, links) = load_in_website(line, base);
		except:
			time.sleep(2.0);
			line = line[0:4]+line[5:];
			print("Trying " + line + " instead. ");
			(text, links) = load_in_website(line, base);
		if not text :
			continue;
		address = find_address(text2list(text));
		tlf = find_tlf(text2list(text));
		email = find_email(text2list(text));
		for link in links[0:10]:
			if address and email and tlf:
				break;
			if not link:
				continue;
			print("Searching: " + link);
			(text, _) = load_in_website(base, link);
			if not text:
				continue;
			if not address:
				address = find_address(text2list(text));
			if not tlf:
				tlf = find_tlf(text2list(text));
			if not email:
				email = find_email(text2list(text));
		printToFile(base, address, email, tlf);

def printToFile(website, address, email, tlf):
	out = open(output, 'a');
	# out = sys.stdout;
	out.write(website[0:-1]);
	out.write(', ');
	if address:
		# out.write("Address: ");
		out.write(address.group(0));
	out.write(', ');
	if tlf:
		# print("Telephone: ");
		out.write(tlf.group(0));
	out.write(', ');
	if email:
		# print("Email: ");
		out.write(email.group(0));
	out.write('\n');

def load_in_website(base, link):
	full_link = urljoin(base, link)
	req = urllib.request.Request(full_link, headers = {'User-Agent':'Mozilla/5.0'});
	try:
		html = urllib.request.urlopen(req);
	except:
		return (None, None);
	content = html.read();

	soup = bs(content, 'html.parser')
	
	text = soup.get_text();
	all_links = [link.get('href') for link in soup.find_all('a')];
	time.sleep(2.0)
	return (text, all_links)

def find_email(text):
	for t in text:
		match = re.search(emailRegex, t);
		if match:
			return match;

def find_address(text):
	for t in text:
		match = re.search(addRegex, t);
		if match:
			return match;

def find_tlf(text):
	for t in text:
		match = re.search(tlfRegex, t);
		if match:
			return match;

def main():
	find_information();
	# if len(sys.argv) >= 2:
		# n = int(sys.argv[1])
	# else:
		# n = 3

	# #for link in file :
	# if len(sys.argv) >= 3:
		# link = sys.argv[2]
	# else:
		# link = "https://www.accenture.dk";

	# # link = "http://www." + name + ".dk";
	# # link = "http://www." + name + ".com";
	# # link = "https://www." + name + ".dk";
	# # link = "https://www." + name + ".com";

	# link_list = search(link, '', n);
	# [print(L) for L in link_list]
	# for L in link_list:
		# search(link, L, n);
		# time.sleep(3);

def search(base, link, n):
	line_list, link_list = load_in_website(base, link);
	# for i in range(len(line_list)):
		# line = line_list[i].lower();
		# if "employees" in line or "medarbejdere" in line:
			# print("Arbejdere: ")
			# print(line_list[(i-n):(i+n+1)])
		# if "denmark" in line or "danmark" in line:
			# print("Adresse: ")
			# print(line_list[(i-n):(i+n+1)])
		# if "+45" in line :
			# print("Telefon: ")
			# print(line_list[(i-n):(i+n+1)])
		# if "@" in line :
			# print("E-mail: ")
			# print(line_list[(i-n):(i+n+1)])
	return link_list;



if __name__ == "__main__":
	main()
